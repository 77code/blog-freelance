<td class="text-end">
    <div class="d-flex justify-content-end flex-shrink-0">
        <a data-status="{{ route('admin.post.status', $model->id) }}" href="#" class="btn-status btn btn-light btn-active-color-primary btn-sm me-1">
            Status
        </a>
        <a href="{{ route('admin.post.edit', $model->id) }}" class="btn btn-light btn-active-color-primary btn-sm me-1">
            Edit
        </a>
        <a data-delete="{{ route('admin.post.destroy', $model->id) }}" href="#" class="btn-delete btn btn-light btn-active-color-primary btn-sm">
            Delete
        </a>
    </div>
</td>