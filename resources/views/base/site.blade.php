<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    @yield('styles')
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,500;0,700;1,300;1,500&family=Poppins:ital,wght@0,300;0,500;0,700;1,300;1,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">
</head>

<body>
    <!-- loading -->
<div class="loading-container">
    <div class="h-100 d-flex align-items-center justify-content-center">
        <ul class="list-unstyled">
            <li>
                <img src="assets/images/placeholder/loading.png" alt="Alternate Text" height="100" />

            </li>
            <li>

                <div class="spinner">
                    <div class="rect1"></div>
                    <div class="rect2"></div>
                    <div class="rect3"></div>
                    <div class="rect4"></div>
                    <div class="rect5"></div>

                </div>

            </li>
            <li>
                <p>Loading</p>
            </li>
        </ul>
    </div>
</div>
<!-- End loading -->
    <!-- Header news -->
    <header class="bg-light">
        <!-- Navbar top -->
        <div class="topbar d-none d-sm-block">
            <div class="container ">
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <div class="topbar-left">
                            <div class="topbar-text">
                                {{ \Carbon\Carbon::now()->toFormattedDateString() }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="list-unstyled topbar-right">
                            <ul class="topbar-link">
                                <li><a href="/about-us" title="">Về chúng tôi</a></li>
                                @if(Auth::check())
                                    @role('admin')
                                        <li><a href="/admin/dashboard" title="">Admin Panel</a></li>
                                    @else
                                        <li><a href="#" title="">{{ auth()->user()->getNameAttribute() }}</a></li>
                                    @endrole
                                    <li><a href="#" data-action="{{ theme()->getPageUrl('logout') }}" data-method="post" data-csrf="{{ csrf_token() }}" data-reload="true">
                                        {{ __('Sign Out') }}
                                    </a></li>
                                @else
                                    <li><a href="login" title="">Đăng nhập/Đăng kí</a></li>
                                @endif
                            </ul>
                            <ul class="topbar-sosmed">
                                <li>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Navbar top -->

        <!-- Navbar -->
        <div class="bg-white">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <figure class="pt-4 pb-4 mb-0 text-center my-auto d-none d-sm-block">
                            <a href="/">
                                <img src="{{ asset('assets/images/placeholder/logo.jpg') }}" alt="" class="img-fluid logo">
                            </a>
                        </figure>
                    </div>

                </div>
            </div>
        </div>

        <!-- Navbar menu  -->
        <div class="navigation-wrap navigation-shadow bg-white">

            <nav class="navbar navbar-hover navbar-expand-lg navbar-soft">
                <div class="container">
                    <div class="offcanvas-header">
                        <div data-toggle="modal" data-target="#modal_aside_right" class="btn-md">
                            <span class="navbar-toggler-icon"></span>
                        </div>
                    </div>
                    <figure class="mb-0 mx-auto d-block d-sm-none">
                        <a href="/">
                            <img src="{{ asset('assets/images/placeholder/logo.jpg') }}" alt="" class="img-fluid logo-mobile">
                        </a>
                    </figure>

                    <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav99">
                <span class="navbar-toggler-icon"></span>
            </button> -->
                    <div class="collapse navbar-collapse justify-content-between" id="main_nav99">
                        <ul class="navbar-nav mx-auto">
                            <li class="nav-item">
                                <a class="nav-link active" href="/" data-toggle="dropdown"> Trang chủ </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link active" href="/about-us"> Về chúng tôi </a>
                            </li>

                            <li class="nav-item dropdown has-megamenu">
                                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> Tin tức </a>
                                <div class="dropdown-menu animate fade-down megamenu mx-auto" role="menu">
                                    <div class="container wrap__mobile-megamenu">
                                        <div class="col-megamenu">
                                            <h5 class="title">Bài viết mới</h5>
                                            <hr>

                                            <div class="top__news__slider">
                                                @foreach($post->take(5) as $postNew)
                                                <div class="item">
                                                    <!-- Post Article -->
                                                    <div class="article__entry">
                                                        <div class="article__image">
                                                            <a href="{{ route('article-detail', [$postNew->id]) }}">
                                                                <img src="{{ Storage::url($postNew->slide_url) }}" alt="" class="img-fluid">
                                                            </a>
                                                        </div>
                                                        <div class="article__content">
                                                            <ul class="list-inline">
                                                                <li class="list-inline-item">
                                                                    <span class="text-primary">
                                                                        by Admin
                                                                    </span>,
                                                                </li>

                                                                <li class="list-inline-item">
                                                                    <span>
                                                                    {{ $postNew->created_at }}
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                            <h5>
                                                                <a href="{{ route('article-detail', [$postNew->id]) }}">
                                                                {{ $postNew->title }}
                                                                </a>
                                                            </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>


                                        </div> <!-- col-megamenu.// -->
                                    </div>
                                </div> <!-- dropdown-mega-menu.// -->
                            </li>
                            <li class="nav-item"><a class="nav-link" href="/contact"> Liên hệ </a></li>
                        </ul>

                        <!-- Search bar.// -->
                        <ul class="navbar-nav ">
                            <li class="nav-item search hidden-xs hidden-sm "> <a class="nav-link" href="#">
                                    <i class="fa fa-search"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- Search content bar.// -->
                        <div class="top-search navigation-shadow">
                            <div class="container">
                                <div class="input-group ">
                                    <form action="#">

                                        <div class="row no-gutters mt-3">
                                            <div class="col">
                                                <input class="form-control border-secondary border-right-0 rounded-0" type="search" value="" placeholder="Search " id="example-search-input4">
                                            </div>
                                            <div class="col-auto">
                                                <a class="btn btn-outline-secondary border-left-0 rounded-0 rounded-right" href="#">
                                                    <i class="fa fa-search"></i>
                                                </a>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Search content bar.// -->
                    </div> <!-- navbar-collapse.// -->
                </div>
            </nav>
        </div>
        <!-- End Navbar menu  -->

        <!-- Navbar sidebar menu  -->
        <div id="modal_aside_right" class="modal fixed-left fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-aside" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="widget__form-search-bar  ">
                            <div class="row no-gutters">
                                <div class="col">
                                    <input class="form-control border-secondary border-right-0 rounded-0" value="" placeholder="Search">
                                </div>
                                <div class="col-auto">
                                    <button class="btn btn-outline-secondary border-left-0 rounded-0 rounded-right">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <nav class="list-group list-group-flush">
                            <ul class="navbar-nav ">
                                <li class="nav-item">
                                    <a class="nav-link active text-dark" href="/" data-toggle="dropdown"> Trang chủ
                                    </a>
                                    
                                </li>
                                

                                <li class="nav-item">
                                    <a class="nav-link active  text-dark" href="/about-us" data-toggle="dropdown"> About
                                    </a>
                                </li>
                                <li class="nav-item"><a class="nav-link  text-dark" href="/contact"> contact </a></li>
                            </ul>

                        </nav>
                    </div>
                    <div class="modal-footer">
                        <p>© 2022 <a href="#" title="Blog">Dự án</a></p>
                    </div>
                </div>
            </div> <!-- modal-bialog .// -->
        </div> <!-- modal.// -->
        @yield('content')
        <section class="wrapper__section p-0">
            <div class="wrapper__section__components">
                <!-- Footer -->
                <footer>
                    <div class="wrapper__footer bg-white">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <div class="wrapper__footer-logo text-center">
                                        <a href="#">
                                            <figure class="mb-4">
                                                <img src="{{ asset('assets/images/placeholder/logo.jpg') }}" alt="" class="img-fluid logo-footer">
                                            </figure>
                                        </a>

                                        <p>
                                        Xa xa, đằng sau những ngọn núi chữ, xa những quốc gia VietNam và USA, có những văn tự mù mịt.
                                        </p>
                                        <p class="mb-0">
                                            <button class="btn btn-social btn-social-o facebook mr-1">
                                                <i class="fa fa-facebook-f"></i>
                                            </button>
                                            <button class="btn btn-social btn-social-o twitter mr-1">
                                                <i class="fa fa-twitter"></i>
                                            </button>

                                            <button class="btn btn-social btn-social-o linkedin mr-1">
                                                <i class="fa fa-linkedin"></i>
                                            </button>
                                            <button class="btn btn-social btn-social-o instagram mr-1">
                                                <i class="fa fa-instagram"></i>
                                            </button>

                                            <button class="btn btn-social btn-social-o youtube mr-1">
                                                <i class="fa fa-youtube"></i>
                                            </button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->
            </div>
        </section>
        <script src="{{ asset('assets/js/index.bundle.js') }}"></script>
        <a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up"></i></a>
        @yield('scripts')
</body>

</html>