@extends('base.site')

@section('content')

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Breadcrumb -->
                <ul class="breadcrumbs bg-light mb-4">
                    <li class="breadcrumbs__item">
                        <a href="/" class="breadcrumbs__url">
                            <i class="fa fa-home"></i> Trang chủ</a>
                    </li>
                    <li class="breadcrumbs__item">
                        <a href="/category" class="breadcrumbs__url">Tin tức</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <aside class="wrapper__list__article ">
                    <h4 class="border_section">Tất cả tin tức</h4>

                    <div class="row">
                        @foreach($post as $value)
                            <div class="col-md-6">
                                <!-- Post Article -->
                                <div class="article__entry">
                                    <div class="article__image">
                                        <a href="{{ route('article-detail', [$value->id]) }}">
                                            <img width="500" height="400" src="{{ Storage::url($value->slide_url) }}" alt="" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="article__content">
                                        <div class="article__category">
                                        {{$value->category->name}}
                                        </div>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <span class="text-primary">
                                                    by Admin
                                                </span>
                                            </li>
                                            <li class="list-inline-item">
                                                <span class="text-dark text-capitalize">
                                                {{ $value->first()->created_at->toFormattedDateString() }}
                                                </span>
                                            </li>

                                        </ul>
                                        <h5>
                                            <a href="{{ route('article-detail', [$value->id]) }}">
                                            {{ Str::limit($value->title) }}
                                            </a>
                                        </h5>
                                        <p>
                                            {{ Str::limit($value->content) }}
                                        </p>
                                        <a href="{{ route('article-detail', [$value->id]) }}" class="btn btn-outline-primary mb-4 text-capitalize"> Đọc thêm</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </aside>
            </div>
            @include('site.sidebar')
            
            <div class="clearfix"></div>
        </div>
        <!-- Pagination -->
        <div class="pagination-area">
            <div class="pagination wow fadeIn animated" data-wow-duration="2s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 2s; animation-delay: 0.5s; animation-name: fadeIn;">
                <!-- <a href="#">
                    «
                </a>
                <a href="#">
                    1
                </a>
                <a class="active" href="#">
                    2
                </a>
                <a href="#">
                    3
                </a>
                <a href="#">
                    4
                </a>
                <a href="#">
                    5
                </a>

                <a href="#">
                    »
                </a> -->
                {{ $post->links('pagination::semantic-ui') }}
            </div>
        </div>
    </div>
</section>
@endsection