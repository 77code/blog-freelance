<div class="col-md-4">
                <div class="sidebar-sticky">
                    <aside class="wrapper__list__article ">
                        <h4 class="border_section">Có thể bạn thích</h4>
                        <div class="wrapper__list__article-small">
                            @foreach($post->take(2) as $post_sidebar)
                            <!-- Post Article -->
                            <div class="article__entry">
                                <div class="article__image">
                                    <a href="{{ route('article-detail', [$post_sidebar->id]) }}">
                                        <img width="500" height="400" src="{{ Storage::url($post_sidebar->slide_url) }}" alt="" class="img-fluid">
                                    </a>
                                </div>
                                <div class="article__content">
                                    <div class="article__category">
                                    {{$post_sidebar->category->name}}
                                    </div>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            <span class="text-primary">
                                                by Admin
                                            </span>
                                        </li>
                                        <li class="list-inline-item">
                                            <span class="text-dark text-capitalize">
                                            {{ $post_sidebar->first()->created_at->toFormattedDateString() }}
                                            </span>
                                        </li>

                                    </ul>
                                    <h5>
                                        <a href="{{ route('article-detail', [$post_sidebar->id]) }}">
                                        {{ Str::limit($post_sidebar->title) }}
                                        </a>
                                    </h5>
                                    <p>
                                    {{ Str::limit($post_sidebar->content) }}
                                    </p>
                                    <a href="{{ route('article-detail', [$post_sidebar->id]) }}" class="btn btn-outline-primary mb-4 text-capitalize"> read more</a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </aside>

                    <aside class="wrapper__list__article">
                        <h4 class="border_section">Thẻ</h4>
                        <div class="blog-tags p-0">
                            <ul class="list-inline">

                                <li class="list-inline-item">
                                    <a href="#">
                                        #property
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        #sea
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        #programming
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        #sea
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        #property
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        #life style
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        #technology
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        #framework
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        #sport
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        #game
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        #wfh
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        #sport
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        #game
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        #wfh
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        #framework
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </aside>

                    <aside class="wrapper__list__article">
                        <h4 class="border_section">Bản tin</h4>
                        <!-- Form Subscribe -->
                        <div class="widget__form-subscribe bg__card-shadow">
                            <h6>
                            Những tin tức và sự kiện thế giới quan trọng nhất trong ngày.
                            </h6>
                            <p><small>Nhận Bản tin hàng ngày Magzrenvi trên Hộp thư đến của bạn.</small></p>
                            <div class="input-group ">
                                <input type="text" class="form-control" placeholder="Your email address">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">Đăng kí</button>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>