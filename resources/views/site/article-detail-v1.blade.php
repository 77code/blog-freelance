@extends('base.site')

@section('content')

<section class="pb-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- breaddcrumb -->
                <!-- Breadcrumb -->
                <ul class="breadcrumbs bg-light mb-4">
                    <li class="breadcrumbs__item">
                        <a href="/" class="breadcrumbs__url">
                            <i class="fa fa-home"></i> Trang chủ</a>
                    </li>
                    <li class="breadcrumbs__item">
                        <a href="/category" class="breadcrumbs__url">Tin tức</a>
                    </li>
                    <li class="breadcrumbs__item breadcrumbs__item--current">
                        {{ Str::limit(create_slug($postDetail->title)) }}
                    </li>
                </ul>
                <!-- end breadcrumb -->
            </div>
            <div class="col-md-8">
                <!-- content article detail -->
                <!-- Article Detail -->
                <div class="wrap__article-detail">
                    <div class="wrap__article-detail-title">
                        <h1>
                            {{ Str::limit($postDetail->title) }}
                        </h1>
                        <h3>
                            {{ $postDetail->title }}
                        </h3>
                    </div>
                    <hr>
                    <div class="wrap__article-detail-info">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <figure class="image-profile">
                                    <img width="800" height="500" src="{{ Storage::url($postDetail->slide_url) }}" alt="">
                                </figure>
                            </li>
                            <li class="list-inline-item">

                                <span>
                                    by
                                </span>
                                <a href="#">
                                    Admin
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <span class="text-dark text-capitalize ml-1">
                                    {{ $postDetail->created_at->toFormattedDateString() }}
                                </span>
                            </li>
                            <li class="list-inline-item">
                                <span class="text-dark text-capitalize">
                                    in
                                </span>
                                <a href="#">
                                    {{ $postDetail->category->name }}
                                </a>


                            </li>
                        </ul>
                    </div>

                    <div class="wrap__article-detail-image mt-4">
                        <figure>
                            <img width="800" height="500" src="{{ Storage::url($postDetail->slide_url) }}" class="img-fluid">
                        </figure>
                    </div>
                    <div class="wrap__article-detail-content">
                        <div class="total-views">
                            <div class="total-views-read">
                                15.k
                                <span>
                                    lượt xem
                                </span>
                            </div>


                            <ul class="list-inline">
                                <span class="share">chia sẻ lên:</span>
                                <li class="list-inline-item">
                                    <a class="btn btn-social-o facebook" href="#">
                                        <i class="fa fa-facebook-f"></i>
                                        <span>facebook</span>
                                    </a>

                                </li>
                                <li class="list-inline-item">
                                    <a class="btn btn-social-o twitter" href="#">
                                        <i class="fa fa-twitter"></i>
                                        <span>twitter</span>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="btn btn-social-o whatsapp" href="#">
                                        <i class="fa fa-whatsapp"></i>
                                        <span>whatsapp</span>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a class="btn btn-social-o telegram" href="#">
                                        <i class="fa fa-telegram"></i>
                                        <span>telegram</span>
                                    </a>
                                </li>

                                <li class="list-inline-item">
                                    <a class="btn btn-linkedin-o linkedin" href="#">
                                        <i class="fa fa-linkedin"></i>
                                        <span>linkedin</span>
                                    </a>
                                </li>

                            </ul>
                        </div>
                        <p class="has-drop-cap-fluid">
                            {{ $postDetail->content }}
                        </p>

                        <!-- Blockquote  -->
                        <blockquote class="block-quote">
                            <p>
                                Đây là một thực tế lâu dài rằng một người đọc sẽ bị phân tâm bởi nội dung có thể đọc được của một trang khi nhìn vào bố cục của nó.
                            </p>
                            <cite>
                                Tom Cruise
                            </cite>
                        </blockquote>
                        <!-- Blockquote -->


                        <h5>Cách khởi nghiệp công nghệ tồn tại mà không có tài trợ</h5>
                        <p>
                            ở xa, phía sau những ngọn núi, cách xa các quốc gia Vokalia và Consonantia, có các văn bản mù. Tách họ sống trong bookmarksgrove ngay tại bờ biển ngữ nghĩa, một ngôn ngữ lớn đại dương. Một dòng sông nhỏ tên là duden chảy vào vị trí của họ và cung cấp nó với regelialia cần thiết.
                        </p>
                    </div>


                </div>
                <!-- end content article detail -->

                <!-- tags -->
                <!-- News Tags -->
                <div class="blog-tags">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <i class="fa fa-tags">
                            </i>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                #property
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                #sea
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                #programming
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                #sea
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                #property
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- end tags-->

                <!-- authors-->
                <!-- Profile author -->
                <div class="wrap__profile">
                    <div class="wrap__profile-author">
                        <figure>
                            <img src="{{ asset('assets/images/placeholder/80x80.jpg') }}" alt="" class="img-fluid rounded-circle">
                        </figure>
                        <div class="wrap__profile-author-detail">
                            <div class="wrap__profile-author-detail-name">tác giả</div>
                            <h4>Trinh Huynh</h4>
                            <p>Slogan: Kết quả của việc tự học và viết lách là một thứ gì đó đáng để suy nghĩ khi bạn chưa có tiền !</p>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="fb.com/trinhdev.net" class="btn btn-social btn-social-o facebook ">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#" class="btn btn-social btn-social-o twitter ">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#" class="btn btn-social btn-social-o instagram ">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#" class="btn btn-social btn-social-o telegram ">
                                        <i class="fa fa-telegram"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#" class="btn btn-social btn-social-o linkedin ">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- end author-->

                <!-- comment -->
                <!-- Comment  -->
                <div id="comments" class="comments-area">
                    <h3 class="comments-title">2 bình luận:</h3>

                    <ol class="comment-list">
                        <li class="comment">
                            <aside class="comment-body">
                                <div class="comment-meta">
                                    <div class="comment-author vcard">
                                        <img src="{{ asset('assets/images/placeholder/80x80.jpg') }}" class="avatar" alt="image">
                                        <b class="fn">Sinmun</b>
                                        <span class="says">says:</span>
                                    </div>

                                    <div class="comment-metadata">
                                        <a href="#">
                                            <span>April 24, 2019 at 10:59 am</span>
                                        </a>
                                    </div>
                                </div>

                                <div class="comment-content">
                                    <p>Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown
                                        printer took a galley of type and scrambled it to make a type specimen book.</p>
                                </div>

                                <div class="reply">
                                    <a href="#" class="comment-reply-link">Reply</a>
                                </div>
                            </aside>

                            <ol class="children">
                                <li class="comment">
                                    <aside class="comment-body">
                                        <div class="comment-meta">
                                            <div class="comment-author vcard">
                                                <img src="{{ asset('assets/images/placeholder/80x80.jpg') }}" class="avatar" alt="image">
                                                <b class="fn">Sinmun</b>
                                                <span class="says">says:</span>
                                            </div>

                                            <div class="comment-metadata">
                                                <a href="#">
                                                    <span>April 24, 2019 at 10:59 am</span>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="comment-content">
                                            <p>Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an
                                                unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                        </div>

                                        <div class="reply">
                                            <a href="#" class="comment-reply-link">Reply</a>
                                        </div>
                                    </aside>
                                </li>
                            </ol>
                        </li>

                        <li class="comment">
                            <aside class="comment-body">
                                <div class="comment-meta">
                                    <div class="comment-author vcard">
                                        <img src="{{ asset('assets/images/placeholder/80x80.jpg') }}" class="avatar" alt="image">
                                        <b class="fn">Sinmun</b>
                                        <span class="says">says:</span>
                                    </div>

                                    <div class="comment-metadata">
                                        <a href="#">
                                            <span>April 24, 2019 at 10:59 am</span>
                                        </a>
                                    </div>
                                </div>

                                <div class="comment-content">
                                    <p>Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown
                                        printer took a galley of type and scrambled it to make a type specimen book.</p>
                                </div>

                                <div class="reply">
                                    <a href="#" class="comment-reply-link">Reply</a>
                                </div>
                            </aside>
                        </li>
                    </ol>

                    <div class="comment-respond">
                        <h3 class="comment-reply-title">Để lại bình luận</h3>

                        <form class="comment-form">
                            <p class="comment-notes">
                                <span id="email-notes">Địa chỉ email của bạn sẽ không được công bố. Các trường bắt buộc được đánh dấu</span>
                                <span class="required">*</span>
                            </p>
                            <p class="comment-form-comment">
                                <label for="comment">Bình luận</label>
                                <textarea name="comment" id="comment" cols="45" rows="5" maxlength="65525" required="required"></textarea>
                            </p>
                            <p class="comment-form-author">
                                <label>Tên <span class="required">*</span></label>
                                <input type="text" id="author" name="name" required="required">
                            </p>
                            <p class="comment-form-email">
                                <label for="email">Email <span class="required">*</span></label>
                                <input type="email" id="email" name="email" required="required">
                            </p>
                            <p class="comment-form-url">
                                <label for="url">Website</label>
                                <input type="url" id="url" name="url">
                            </p>
                            <p class="comment-form-cookies-consent">
                                <input type="checkbox" value="yes" name="wp-comment-cookies-consent" id="wp-comment-cookies-consent">
                                <label for="wp-comment-cookies-consent">Lưu tên, email và trang web của tôi trong trình duyệt này cho span tiếp theo tôi nhận xét.</label>
                            </p>
                            <p class="form-submit">
                                <input type="submit" name="submit" id="submit" class="submit" value="Đăng bình luận">
                            </p>
                        </form>
                    </div>
                </div>
                <!-- Comment -->
                <!-- end comment -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="single_navigation-prev">
                            <a href="#">
                                <span>Bài viết trước</span>
                                {{Str::limit($post->first()->title)}}
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="single_navigation-next text-left text-md-right">
                            <a href="#">
                                <span>Bài viết sau</span>
                                {{Str::limit($post->last()->title)}}
                            </a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="related-article">
                    <h4>
                        Có thể bạn thích
                    </h4>

                    <div class="article__entry-carousel-three">
                        @foreach($post as $value)
                        <div class="item">
                            <!-- Post Article -->
                            <div class="article__entry">
                                <div class="article__image">
                                    <a href="#">
                                        <img width="500" height="400" src="{{ Storage::url($value->slide_url) }}" alt="" class="img-fluid">
                                    </a>
                                </div>
                                <div class="article__content">
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            <span class="text-primary">
                                                by Admin
                                            </span>
                                        </li>
                                        <li class="list-inline-item">
                                            <span>
                                                {{ $value->created_at->toFormattedDateString() }}
                                            </span>
                                        </li>

                                    </ul>
                                    <h5>
                                        <a href="#">
                                            {{ Str::limit($value->title, 40) }}
                                        </a>
                                    </h5>

                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

            </div>
            @include('site.sidebar')
        </div>
    </div>
</section>
@endsection