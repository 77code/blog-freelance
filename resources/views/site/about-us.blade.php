@extends('base.site')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- breadcrumb -->
                <!-- Breadcrumb -->
                <ul class="breadcrumbs bg-light mb-4">
                    <li class="breadcrumbs__item">
                        <a href="/" class="breadcrumbs__url">
                            <i class="fa fa-home"></i> Trang chủ</a>
                    </li>
                    <li class="breadcrumbs__item">
                        <a href="/about-us" class="breadcrumbs__url">Về chúng tôi</a>
                    </li>
                </ul>
                <!-- End breadcrumb -->

                <div class="wrap__about-us">
                    <h2>Nhiệm vụ của chúng ta</h2>
                    <p>Cố gắng hoàn thành dự án nhanh nhất có thể, <b>THANH DUY</b>
                    </p>

                    <figure class="float-left mr-3">
                        <img width="500" height="400" src="{{ Storage::url($post->first()->slide_url) }}" class="img-fluid">
                    </figure>
                    <p>
Khi đại dịch toàn cầu hoành hành, tất cả chúng ta đều trở thành nhân chứng sống cho những giai đoạn ẩn dật không thể tưởng tượng nổi. Con người ngày càng trở nên xa cách với nhau hơn. Sự cô lập liên tục này làm cho nhu cầu lướt web và sử dụng internet của mọi người nhiều hơn, ở nơi mọi người có thể yêu cầu tạo cho mình một website theo suy nghĩ của mình để bắt đầu kinh doanh một cách sớm nhất.
 là một hệ thống để phát triển các website giàu trí tưởng tượng và gây nghiện nhất. Chúng tôi đang cung cấp cho cộng đồng website của mình trải nghiệm gần như tốt nhất với yêu cầu của họ. Hệ sinh thái của chúng tôi sẽ là một hệ thống tự tạo website trong tương lai, đặc biệt dành cho những người thích sự nhanh gọn nhưng vẫn thấy được chất lượng trong đó.

Khi đại dịch toàn cầu hoành hành, tất cả chúng ta đều trở thành nhân chứng sống cho những giai đoạn ẩn dật không thể tưởng tượng nổi. Con người ngày càng trở nên xa cách với nhau hơn. Sự cô lập liên tục này làm cho nhu cầu lướt web và sử dụng internet của mọi người nhiều hơn, ở nơi mọi người có thể yêu cầu tạo cho mình một website theo suy nghĩ của mình để bắt đầu kinh doanh một cách sớm nhất.
                    </p>
                    <div class="clearfix"></div>
                    
            </div>


        </div>
    </div>
</section>
@endsection