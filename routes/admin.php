<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Administrator\PostController;
use App\Http\Controllers\Administrator\AdminController;
use App\Http\Controllers\Administrator\PagesController;
use App\Http\Controllers\Administrator\CategoryController;
use App\Http\Controllers\Administrator\Logs\AuditLogsController;
use App\Http\Controllers\Administrator\Logs\SystemLogsController;
use App\Http\Controllers\Administrator\Account\SettingsController;

Route::middleware('role:admin')->group(function () {
    Route::get('/dashboard', [AdminController::class, 'admin'])->middleware('auth');

    $menu = theme()->getMenu();
    array_walk($menu, function ($val) {
        if (isset($val['path'])) {
            $route = Route::get($val['path'], [PagesController::class, 'index']);

            // Exclude documentation from auth middleware
            if (!Str::contains($val['path'], 'documentation')) {
                $route->middleware('auth');
            }
        }
    });
    // Account pages
    Route::prefix('account')->group(function () {
        Route::get('settings', [SettingsController::class, 'index'])->name('settings.index');
        Route::get('overview', [SettingsController::class, 'overview'])->name('settings.overview');
        Route::put('settings', [SettingsController::class, 'update'])->name('settings.update');
        Route::put('settings/email', [SettingsController::class, 'changeEmail'])->name('settings.changeEmail'); 
        Route::put('settings/password', [SettingsController::class, 'changePassword'])->name('settings.changePassword');
        Route::post('settings', [SettingsController::class, 'statistical']);
    });

    // Category account pages
    Route::prefix('category')->group(function () {
        Route::get('index', [CategoryController::class, 'index'])->name('category.index');
        Route::get('add', [CategoryController::class, 'create'])->name('category.create');
        Route::get('edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');

        Route::post('add', [CategoryController::class, 'store'])->name('category.store');
        Route::put('edit/{id}', [CategoryController::class, 'update'])->name('category.update');
        Route::post('destroy/{id}', [CategoryController::class, 'destroy'])->name('category.destroy');
        Route::post('status/{id}', [CategoryController::class, 'update_status'])->name('category.status');
    });

    // Account game pages
    Route::prefix('post')->group(function () {
        Route::get('index', [PostController::class, 'index'])->name('post.index');
        Route::get('add', [PostController::class, 'create'])->name('post.create');
        Route::get('edit/{id}', [PostController::class, 'edit'])->name('post.edit');

        Route::post('add', [PostController::class, 'store'])->name('post.store');
        Route::put('edit/{id}', [PostController::class, 'update'])->name('post.update');
        Route::post('destroy/{id}', [PostController::class, 'destroy'])->name('post.destroy');
        Route::post('status/{id}', [PostController::class, 'update_status'])->name('post.status');
    });
    // Logs pages
    Route::prefix('log')->group(function () {
        Route::resource('system', SystemLogsController::class)->only(['index', 'destroy']);
        Route::resource('audit', AuditLogsController::class)->only(['index', 'destroy']);
    });

    //Setting
    Route::prefix('setting')->group(function () {
        Route::get('index', [SettingController::class, 'edit'])->name('setting.edit');
        Route::post('update', [SettingController::class, 'update'])->name('setting.update');
    });
});
        


