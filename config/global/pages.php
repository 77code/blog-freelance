<?php
return array(
    'admin' => array(  
        'dashboard' => array(
            'title'       => 'Dashboard',
            'description' => '',
            'view'        => 'index',
            'layout'      => array(
                'page-title' => array(
                    'description' => true,
                    'breadcrumb'  => false,
                ),
            ),
            'assets'      => array(
                'custom' => array(
                    'js' => array(),
                ),
            ),
        ),

        'login'           => array(
            'title'  => 'Login',
            'assets' => array(
                'custom' => array(
                    'js' => array(
                        'js/custom/authentication/sign-in/general.js',
                    ),
                ),
            ),
            'layout' => array(
                'main' => array(
                    'type' => 'blank', // Set blank layout
                    'body' => array(
                        'class' => theme()->isDarkMode() ? '' : 'bg-body',
                    ),
                ),
            ),
        ),
        'register'        => array(
            'title'  => 'Register',
            'assets' => array(
                'custom' => array(
                    'js' => array(
                        'js/custom/authentication/sign-up/general.js',
                    ),
                ),
            ),
            'layout' => array(
                'main' => array(
                    'type' => 'blank', // Set blank layout
                    'body' => array(
                        'class' => theme()->isDarkMode() ? '' : 'bg-body',
                    ),
                ),
            ),
        ),

        'log' => array(
            'audit'  => array(
                'title'  => 'Audit Log',
                'assets' => array(
                    'custom' => array(
                        'css' => array(
                            'plugins/custom/datatables/datatables.bundle.css',
                        ),
                        'js'  => array(
                            'plugins/custom/datatables/datatables.bundle.js',
                        ),
                    ),
                ),
            ),
            'system' => array(
                'title'  => 'System Log',
                'assets' => array(
                    'custom' => array(
                        'css' => array(
                            'plugins/custom/datatables/datatables.bundle.css',
                        ),
                        'js'  => array(
                            'plugins/custom/datatables/datatables.bundle.js',
                        ),
                    ),
                ),
            ),
        ),

        'account' => array(
            'overview' => array(
                'title'  => 'Account Overview',
                'view'   => 'account/overview/overview',
                'assets' => array(
                    'custom' => array(
                        'js' => array(
                            'js/custom/widgets.js',
                        ),
                    ),
                ),
            ),

            'settings' => array(
                'title'  => 'Account Settings',
                'assets' => array(
                    'custom' => array(
                        'js' => array(
                            'js/custom/account/settings/profile-details.js',
                            'js/custom/account/settings/signin-methods.js',
                            'js/custom/modals/two-factor-authentication.js',
                        ),
                    ),
                ),
            ),
        ),

        'category' => array(
            'index' => array(
                'title'  => 'Category',
                'view'   => 'admin/category/index',
                'assets' => array(
                    'custom' => array(
                        'css' => array(
                            'plugins/custom/datatables/datatables.bundle.css',
                        ),
                        'js' => array(
                            'plugins/custom/datatables/datatables.bundle.js',
                        )
                    )
                ),
            ),
            'add' => array(
                'title'  => 'Category',
                'view'   => 'account/category/add',
                'assets' => array(
                    'custom' => array(
                        'css' => array(
                            'plugins/custom/datatables/datatables.bundle.css',
                        ),
                        'js' => array(
                            'plugins/custom/datatables/datatables.bundle.js',
                        )
                    )
                ),
            ),

            'edit' => array(
                'title'  => 'Category'
            ),
        ),

        'post' => array(
            'index' => array(
                'title'  => 'post',
                'view'   => 'admin/post/index',
                'assets' => array(
                    'custom' => array(
                        'css' => array(
                            'plugins/custom/datatables/datatables.bundle.css',
                        ),
                        'js' => array(
                            'plugins/custom/datatables/datatables.bundle.js',
                        )
                    )
                ),
            ),
            'add' => array(
                'title'  => 'post',
                'view'   => 'account/post/add',
                'assets' => array(
                    'custom' => array(
                        'css' => array(
                            'plugins/custom/datatables/datatables.bundle.css',
                        ),
                        'js' => array(
                            'plugins/custom/datatables/datatables.bundle.js',
                        )
                    )
                ),
            ),

            'edit' => array(
                'title'  => 'post'
            ),
        ),

        
    ),
);
