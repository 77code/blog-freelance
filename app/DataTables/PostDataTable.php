<?php

namespace App\DataTables;

use App\Models\Post;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('slide_url', function (Post $model) {
                return '<img src="'.Storage::url($model->slide_url).'" alt="" class="symbol w-75px h-75px">';
            })
            ->editColumn('category_id', function (Post $model) {
                return $model->category->name ?? 'null';
            })
            ->editColumn('status', function (Post $model) {
                if ($model->status == 1) {
                    $style = 'danger';
                    $value = 'PRIVATE';
                }else{
                    $style = 'success';
                    $value = 'PUBLIC';
                }
                return '<div class="badge badge-light-'.$style.' fw-bolder">'.$value.'</div>';
            })
            ->editColumn('content', function (Post $model) {
                $content = $model->content;

                return view('admin.post._details', compact('content'));
            })
            ->rawColumns(['slide_url','status','action'])
            ->addColumn('action', 'admin.Post._action-menu');
           
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Post $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('post-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->responsive()
                    ->autoWidth(false)
                    ->parameters([
                        'scrollX'      => true
                    ])
                    ->addTableClass('align-middle table-row-dashed fs-6 gy-5');
                    
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            
            Column::make('id')->title('ID')->width(100)->addClass('ps-0'),
            Column::make('slide_url')->title('Hình ảnh'),
            Column::make('title')->title('Tiêu đề'),
            Column::make('category_id')->title('Thể loại'),
            Column::make('content')->title('Nội dung')->addClass('none'),
            Column::make('status')->title('Trạng thái'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->title('Hành động')
                  ->addClass('text-center'),
            Column::make('created_at')->title('Ngày tạo')->addClass('none'),
            Column::make('updated_at')->title('Ngày sửa đổi')->addClass('none'),
            
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Post_' . date('YmdHis');
    }
}
