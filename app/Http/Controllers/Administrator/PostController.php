<?php

namespace App\Http\Controllers\Administrator;

use App\Models\Post;
use Illuminate\Http\Request;
use App\DataTables\PostDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PostDataTable $dataTable)
    {
        return $dataTable->render('admin.post.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::where('id', $id)->first();
        return view('admin.post._edit', compact('post'));
    }

    public function create()
    {
        return view('admin.post._add');
    }

    public function update(Request $request, $id)
    {
        
        $data = request()->validate([
            'title'        => 'required|string|max:255',
            'category_id'       => 'required',
            'content'        => 'required|string',
            'slide_url'       => 'required',
            'status'      => 'required',
        ]);
        
        $post = Post::findOrFail($id);
        
        // include to save image
        if ($image = $this->upload()) {
            $post->image = $image;
        }

        $post->update([
            'title' => $data['title'],
            'category_id' =>$data['category_id'],
            'content' => $data['content'],
            'status' => $data['status'],
            'slide_url' => $image,
        ]);
       
        return redirect()->intended('admin/post/index')->with('success', 'Thành công');
    }

    public function store(Request $request)
    {
        
        $data = request()->validate([
            'title'        => 'required|string|max:255',
            'category_id'       => 'required',
            'content'        => 'required|string',
            'slide_url'       => 'required',
            'status'      => 'required',
        ]);
        
        $post = new Post();
        
        // include to save image
        if ($image = $this->upload()) {
            $post->image = $image;
        }

        $post->create([
            'title' => $data['title'],
            'category_id' =>$data['category_id'],
            'content' => $data['content'],
            'status' => $data['status'],
            'slide_url' => $image,
        ]);
       
        return redirect()->intended('admin/post/index');
    }

    public function upload($folder = 'images', $key = 'slide_url', $validation = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|sometimes')
    {
        request()->validate([$key => $validation]);

        $file = null;
        if (request()->hasFile($key)) {
            $file = Storage::disk('public')->putFile($folder, request()->file($key), 'public');
        }

        return $file;
    }

    //DELETE
    public function destroy($id)
    {
        $post = Post::destroy($id);
        if($post){
            Storage::disk('public')->delete($post->slide_url);
        }
        return redirect()->back();
    }

    //UPDATE STATUS
    public function update_status($id)
    {
        $post = Post::find($id);
        if($post->status == 0)
        {
            $post->status = 1;
        }else {
            $post->status = 0;
        }
        $post->save();
        return redirect()->back();
    }
}
