<?php
namespace App\Http\Controllers\Administrator;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function admin()
    {
        return view('admin.index');
    }
}