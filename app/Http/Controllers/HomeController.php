<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function index()
    {
        return view('site.index');
    }
 
    public function article_detail($id)
    {
        $postDetail = Post::find($id);
        return view('site.article-detail-v1', compact('postDetail'));
    }

    public function contact()
    {
        return view('site.contact');
    }

    public function category()
    {
        return view('site.category-style-v1');
    }

    public function about()
    {
        return view('site.about-us');
    }
}
